package com.roensby.sidebag;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HistoryListener implements View.OnLongClickListener {

    JSONObject book;

    CharacterSheet context;

    String characterName;

    public HistoryListener(JSONObject incomingBook, CharacterSheet incomingContext, String incomingCharacterName) {
        book = incomingBook;
        context = incomingContext;
        characterName = incomingCharacterName;
    }

    @Override
    public boolean onLongClick(View view) {
        String target = context.getResources().getResourceEntryName(view.getId()).toLowerCase();
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        LayoutInflater inflater = context.getLayoutInflater();
        View dialog_history = inflater.inflate(R.layout.dialog_history, null);
        dialog.setView(dialog_history);
        try {
            JSONObject characterLedger = (JSONObject) book.get(characterName);
            if (!characterLedger.has(target)) {
                characterLedger.put(target, new JSONArray());
            }
            JSONArray targetLedger = (JSONArray) characterLedger.get(target);
            ListView listView = dialog_history.findViewById(R.id.history_list);
            ListAdapter adapter = new HistoryViewAdapter(context, targetLedger);
            listView.setAdapter(adapter);
            dialog.setView(dialog_history);
            dialog.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }
}
