package com.roensby.sidebag;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SmallNumbersListener implements View.OnClickListener {

    JSONObject book;

    CharacterSheet context;

    AlertDialog dialog;

    String characterName;

    String target;

    public SmallNumbersListener(CharacterSheet incomingContext, AlertDialog incomingDialog, JSONObject incomingBook, String incomingCharacterName) {
        book = incomingBook;
        context = incomingContext;
        dialog = incomingDialog;
        characterName = incomingCharacterName;
    }

    @Override
    public void onClick(View view) {
        int buttonId = view.getId();
        TextView smallNumberValue = dialog.findViewById(R.id.smallNumberValue);
        int value = Integer.parseInt(smallNumberValue.getText().toString());
        if (buttonId == R.id.smallNumberAdd) {
            value = value + 1;
            smallNumberValue.setText(String.valueOf(value));
        }
        else if (buttonId == R.id.smallNumberSubtract) {
            value = value - 1;
            smallNumberValue.setText(String.valueOf(value));
        }
        else if (buttonId == R.id.smallNumberClose) {
            try {
                JSONObject entry = new JSONObject();
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(new Date());
                entry.put("date", date);
                entry.put("type", "overwrite");
                entry.put("value", value);
                if (!book.has(characterName)) {
                    book.put(characterName, new JSONObject());
                }
                JSONObject characterLedger = (JSONObject) book.get(characterName);
                if (!characterLedger.has(target)) {
                    characterLedger.put(target, new JSONArray());
                }
                JSONArray targetLedger = (JSONArray) characterLedger.get(target);
                targetLedger.put(entry);
                writeToFile(book.toString(), context);
                context.refreshValues();
                dialog.dismiss();
            } catch (JSONException e) {
                Log.e("SmallNumber", "JSON error: " + e.toString());
            }
        }
    }

    public void setTarget(String incomingTarget) {
        target = incomingTarget;
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("sidebag.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
