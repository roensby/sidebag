package com.roensby.sidebag;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HistoryViewAdapter extends BaseAdapter {

    CharacterSheet context;

    JSONArray values;

    public HistoryViewAdapter(CharacterSheet context, JSONArray values) {
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return values.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int currentPosition, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View historyEntry = inflater.inflate(R.layout.list_item_history, null);
        TextView entryDate = historyEntry.findViewById(R.id.entryDate);
        TextView entryType = historyEntry.findViewById(R.id.entryType);
        TextView entryValue = historyEntry.findViewById(R.id.entryValue);
        Button entryButton = historyEntry.findViewById(R.id.entryButton);
        entryButton.setOnClickListener(confirmDelete);
        historyEntry.setTag(currentPosition);
        try {
            JSONObject entry = values.getJSONObject(currentPosition);
            entryDate.setText(entry.get("date").toString());
            entryType.setText(entry.get("type").toString().toUpperCase());
            entryValue.setText(entry.get("value").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return historyEntry;
    }

    private final View.OnClickListener confirmDelete = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            View historyEntry = (View) view.getParent().getParent();
            int position = (int) historyEntry.getTag();
            AlertDialog confirmDeleteDialog = new AlertDialog.Builder(context).create();
            confirmDeleteDialog.setTitle("Are you sure?");
            confirmDeleteDialog.setMessage("Please confirm deletion");
            confirmDeleteDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    values.remove(position);
                    notifyDataSetChanged();
                    try {
                        context.refreshValues();
                    } catch (JSONException e) {
                        Log.e("History", "Failed to refresh values: " + e.toString());
                    }
                    dialog.dismiss();
                }
            });
            confirmDeleteDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            confirmDeleteDialog.show();
        }
    };
}
