package com.roensby.sidebag;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BigNumbersListener implements View.OnClickListener {

    JSONObject book;

    CharacterSheet context;

    AlertDialog dialog;

    String characterName;

    String target;

    public BigNumbersListener(CharacterSheet incomingContext, AlertDialog incomingDialog, JSONObject incomingBook, String incomingCharacterName) {
        book = incomingBook;
        context = incomingContext;
        dialog = incomingDialog;
        characterName = incomingCharacterName;
    }

    @Override
    public void onClick(View view) {
        int buttonId = view.getId();
        if (buttonId != R.id.bigNumberCancel) {
            View parentView = (View) view.getParent();
            EditText bigNumberInput = parentView.findViewById(R.id.bigNumberInput);
            String inputValue = bigNumberInput.getText().toString();
            if (!inputValue.isEmpty()) {
                try {
                    JSONObject entry = new JSONObject();
                    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(new Date());
                    entry.put("date", date);
                    entry.put("type", "add");
                    entry.put("value", Integer.valueOf(inputValue));
                    if (buttonId == R.id.bigNumberAdd) {
                        entry.put("type", "add");
                    }
                    if (buttonId == R.id.bigNumberSubtract) {
                        entry.put("type", "subtract");
                    }
                    if (buttonId == R.id.bigNumberOverwrite) {
                        entry.put("type", "overwrite");

                    }
                    if (!book.has(characterName)) {
                        book.put(characterName, new JSONObject());
                    }
                    JSONObject characterLedger = (JSONObject) book.get(characterName);
                    if (!characterLedger.has(target)) {
                        characterLedger.put(target, new JSONArray());
                    }
                    JSONArray targetLedger = (JSONArray) characterLedger.get(target);
                    targetLedger.put(entry);
                    Log.d("Ledger", "Book: " + book.toString());
                    writeToFile(book.toString(), context);
                    context.refreshValues();
                    dialog.dismiss();
                } catch (JSONException e) {
                    Log.e("BigNumber", "JSON error: " + e.toString());
                }
            }
            else {
                Log.e("BigNumber", "Input is empty");
                dialog.cancel();
            }
        }
        else {
            dialog.cancel();
        }
    }

    public void setTarget(String incomingTarget) {
        target = incomingTarget;
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("sidebag.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
