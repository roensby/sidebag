package com.roensby.sidebag;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CharacterSheet extends AppCompatActivity {

    JSONObject book;

    String characterName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        characterName = bundle.getString("characterName", "Default name");
        setContentView(R.layout.activity_character_sheet);
        try {
            book = new JSONObject(bundle.getString("book"));
            // Get all buttons.
            Button xp = findViewById(R.id.XP);
            Button gold = findViewById(R.id.Gold);
            Button grit = findViewById(R.id.Grit);
            Button wound = findViewById(R.id.Wound);
            Button horror = findViewById(R.id.Horror);
            Button corruption = findViewById(R.id.Corruption);
            Button level = findViewById(R.id.Level);
            Button agility = findViewById(R.id.Agility);
            Button cunning = findViewById(R.id.Cunning);
            Button spirit = findViewById(R.id.Spirit);
            Button strength = findViewById(R.id.Strength);
            Button lore = findViewById(R.id.Lore);
            Button luck = findViewById(R.id.Luck);
            Button range = findViewById(R.id.Range);
            Button melee = findViewById(R.id.Melee);
            Button combat = findViewById(R.id.Combat);
            Button initiative = findViewById(R.id.Initiative);
            Button health = findViewById(R.id.Health);
            Button defense = findViewById(R.id.Defense);
            Button sanity = findViewById(R.id.Sanity);
            Button willpower = findViewById(R.id.Willpower);
            Button maxgrit = findViewById(R.id.MaxGrit);
            Button maxcorruption = findViewById(R.id.MaxCorruption);
            // Attach listeners to value buttons.
            HistoryListener history = new HistoryListener(book, this, characterName);
            xp.setOnLongClickListener(history);
            xp.setOnClickListener(openBigNumberDialog);
            gold.setOnLongClickListener(history);
            gold.setOnClickListener(openBigNumberDialog);
            grit.setOnLongClickListener(history);
            grit.setOnClickListener(openSmallNumberDialog);
            wound.setOnLongClickListener(history);
            wound.setOnClickListener(openSmallNumberDialog);
            horror.setOnLongClickListener(history);
            horror.setOnClickListener(openSmallNumberDialog);
            corruption.setOnLongClickListener(history);
            corruption.setOnClickListener(openSmallNumberDialog);
            level.setOnLongClickListener(history);
            level.setOnClickListener(openSmallNumberDialog);
            agility.setOnLongClickListener(history);
            agility.setOnClickListener(openSmallNumberDialog);
            cunning.setOnLongClickListener(history);
            cunning.setOnClickListener(openSmallNumberDialog);
            spirit.setOnLongClickListener(history);
            spirit.setOnClickListener(openSmallNumberDialog);
            strength.setOnLongClickListener(history);
            strength.setOnClickListener(openSmallNumberDialog);
            lore.setOnLongClickListener(history);
            lore.setOnClickListener(openSmallNumberDialog);
            luck.setOnLongClickListener(history);
            luck.setOnClickListener(openSmallNumberDialog);
            range.setOnLongClickListener(history);
            range.setOnClickListener(openSmallNumberDialog);
            melee.setOnLongClickListener(history);
            melee.setOnClickListener(openSmallNumberDialog);
            combat.setOnLongClickListener(history);
            combat.setOnClickListener(openSmallNumberDialog);
            initiative.setOnLongClickListener(history);
            initiative.setOnClickListener(openSmallNumberDialog);
            health.setOnLongClickListener(history);
            health.setOnClickListener(openSmallNumberDialog);
            defense.setOnLongClickListener(history);
            defense.setOnClickListener(openSmallNumberDialog);
            sanity.setOnLongClickListener(history);
            sanity.setOnClickListener(openSmallNumberDialog);
            willpower.setOnLongClickListener(history);
            willpower.setOnClickListener(openSmallNumberDialog);
            maxgrit.setOnLongClickListener(history);
            maxgrit.setOnClickListener(openSmallNumberDialog);
            maxcorruption.setOnLongClickListener(history);
            maxcorruption.setOnClickListener(openSmallNumberDialog);
            refreshValues();
        } catch (JSONException e) {
            Log.e("Initialization", e.toString());
        }
    }

    private final View.OnClickListener openBigNumberDialog = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonId = view.getId();
            AlertDialog dialog = new AlertDialog.Builder(CharacterSheet.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogBigNumbers = inflater.inflate(R.layout.dialog_big_numbers, null);
            dialog.setView(dialogBigNumbers);
            BigNumbersListener buttonListener = new BigNumbersListener(CharacterSheet.this, dialog, book, characterName);
            Button bigNumberAdd = dialogBigNumbers.findViewById(R.id.bigNumberAdd);
            bigNumberAdd.setOnClickListener(buttonListener);
            Button bigNumberSubtract = dialogBigNumbers.findViewById(R.id.bigNumberSubtract);
            bigNumberSubtract.setOnClickListener(buttonListener);
            Button bigNumberOverwrite = dialogBigNumbers.findViewById(R.id.bigNumberOverwrite);
            bigNumberOverwrite.setOnClickListener(buttonListener);
            Button bigNumberCancel = dialogBigNumbers.findViewById(R.id.bigNumberCancel);
            bigNumberCancel.setOnClickListener(buttonListener);
            if (buttonId == R.id.XP) {
                buttonListener.setTarget("xp");
            } else if (buttonId == R.id.Gold) {
                buttonListener.setTarget("gold");
            }
            EditText bigNumberInput = dialogBigNumbers.findViewById(R.id.bigNumberInput);
            bigNumberInput.requestFocus();
            bigNumberInput.postDelayed(() -> {
                InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(bigNumberInput,0);
            },200);
            dialog.show();
        }
    };

    private final View.OnClickListener openSmallNumberDialog = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String target = getResources().getResourceEntryName(view.getId()).toLowerCase();
            Log.d("entryname", target);
            AlertDialog dialog = new AlertDialog.Builder(CharacterSheet.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogSmallNumbers = inflater.inflate(R.layout.dialog_small_numbers, null);
            dialog.setView(dialogSmallNumbers);
            SmallNumbersListener buttonListener = new SmallNumbersListener(CharacterSheet.this, dialog, book, characterName);
            Button smallNumberAdd = dialogSmallNumbers.findViewById(R.id.smallNumberAdd);
            smallNumberAdd.setOnClickListener(buttonListener);
            Button smallNumberSubtract = dialogSmallNumbers.findViewById(R.id.smallNumberSubtract);
            smallNumberSubtract.setOnClickListener(buttonListener);
            Button smallNumberClose = dialogSmallNumbers.findViewById(R.id.smallNumberClose);
            smallNumberClose.setOnClickListener(buttonListener);
            TextView smallNumberValue = dialogSmallNumbers.findViewById(R.id.smallNumberValue);
            buttonListener.setTarget(target);
            try {
                if (!book.has(characterName)) {
                    book.put(characterName, new JSONObject());
                }
                JSONObject characterLedger = (JSONObject) book.get(characterName);
                if (!characterLedger.has(target)) {
                    characterLedger.put(target, new JSONArray());
                }
                JSONArray targetLedger = (JSONArray) characterLedger.get(target);
                int value = 0;
                if (targetLedger.length() > 0) {
                    JSONObject entry = (JSONObject) targetLedger.get(targetLedger.length() - 1);
                    value = Integer.parseInt(entry.get("value").toString());
                }
                smallNumberValue.setText(String.valueOf(value));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.show();
        }
    };

    @SuppressLint("SetTextI18n")
    public void refreshValues() throws JSONException {
        // Get all buttons.
        Button xp = findViewById(R.id.XP);
        Button gold = findViewById(R.id.Gold);
        Button grit = findViewById(R.id.Grit);
        Button wound = findViewById(R.id.Wound);
        Button horror = findViewById(R.id.Horror);
        Button corruption = findViewById(R.id.Corruption);
        Button level = findViewById(R.id.Level);
        Button agility = findViewById(R.id.Agility);
        Button cunning = findViewById(R.id.Cunning);
        Button spirit = findViewById(R.id.Spirit);
        Button strength = findViewById(R.id.Strength);
        Button lore = findViewById(R.id.Lore);
        Button luck = findViewById(R.id.Luck);
        Button range = findViewById(R.id.Range);
        Button melee = findViewById(R.id.Melee);
        Button combat = findViewById(R.id.Combat);
        Button initiative = findViewById(R.id.Initiative);
        Button health = findViewById(R.id.Health);
        Button defense = findViewById(R.id.Defense);
        Button sanity = findViewById(R.id.Sanity);
        Button willpower = findViewById(R.id.Willpower);
        Button maxgrit = findViewById(R.id.MaxGrit);
        Button maxcorruption = findViewById(R.id.MaxCorruption);
        TextView characterNameLabel = findViewById(R.id.characterNameLabel);
        TextView classLabel = findViewById(R.id.classLabel);
        // Set values for entryButton labels.
        xp.setText(Integer.toString(calculateSum(xp)));
        gold.setText(Integer.toString(calculateSum(gold)));
        grit.setText(Integer.toString(calculateSum(grit)));
        wound.setText(Integer.toString(calculateSum(wound)));
        horror.setText(Integer.toString(calculateSum(horror)));
        corruption.setText(Integer.toString(calculateSum(corruption)));
        level.setText(Integer.toString(calculateSum(level)));
        agility.setText(Integer.toString(calculateSum(agility)));
        cunning.setText(Integer.toString(calculateSum(cunning)));
        spirit.setText(Integer.toString(calculateSum(spirit)));
        strength.setText(Integer.toString(calculateSum(strength)));
        lore.setText(Integer.toString(calculateSum(lore)));
        luck.setText(Integer.toString(calculateSum(luck)));
        range.setText(calculateSum(range) + "+");
        melee.setText(calculateSum(melee) + "+");
        combat.setText(Integer.toString(calculateSum(combat)));
        initiative.setText(Integer.toString(calculateSum(initiative)));
        health.setText(Integer.toString(calculateSum(health)));
        defense.setText(calculateSum(defense) + "+");
        sanity.setText(Integer.toString(calculateSum(sanity)));
        willpower.setText(calculateSum(willpower) + "+");
        maxgrit.setText(Integer.toString(calculateSum(maxgrit)));
        maxcorruption.setText(Integer.toString(calculateSum(maxcorruption)));
        characterNameLabel.setText(characterName);
        JSONObject characterSheet = (JSONObject) book.get(characterName);
        classLabel.setText(characterSheet.get("class").toString());
    }

    private int calculateSum(View view) throws JSONException {
        String target = getResources().getResourceEntryName(view.getId()).toLowerCase();
        int value = 0;
        if (!book.has(characterName)) {
            book.put(characterName, new JSONObject());
        }
        JSONObject characterSheet = (JSONObject) book.get(characterName);
        if (!characterSheet.has(target)) {
            characterSheet.put(target, new JSONArray());
        }
        JSONArray targetLedger = (JSONArray) characterSheet.get(target);
        if (targetLedger.length() > 0) {
            for (int i = 0; i < targetLedger.length(); i++) {
                JSONObject entry = targetLedger.getJSONObject(i);
                switch ((String) entry.get("type")) {
                    case "add":
                        value += (int) entry.get("value");
                        break;

                    case "subtract":
                        value -= (int) entry.get("value");
                        break;

                    case "overwrite":
                        value = (int) entry.get("value");
                        break;
                }
            }
        }
        return value;
    }
}
