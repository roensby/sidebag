package com.roensby.sidebag;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Start extends AppCompatActivity {

    JSONObject book;

    private List<String> characterNames;

    private Boolean spinnerTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Button createCharacterName = findViewById(R.id.createNewCharacter);
        createCharacterName.setOnClickListener(openCharacterNameDialog);
        try {
            String data = readFromFile();
            book = data.isEmpty() ? new JSONObject() : new JSONObject(data);
            characterNames = new ArrayList<String>();
            populateCharacterNames();
            Spinner spinner = findViewById(R.id.characterNames);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, characterNames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(spinnerOnItemSelect);
            spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    spinnerTouched = true;
                    return false;
                }
            });
        } catch (JSONException e) {
            Log.e("Initialization", "File read failed: " + e.toString());
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        try {
            String data = readFromFile();
            book = data.isEmpty() ? new JSONObject() : new JSONObject(data);
            characterNames.clear();
            populateCharacterNames();
        } catch (JSONException e) {
            Log.e("Initialization", "File read failed: " + e.toString());
        }
    }

    private void populateCharacterNames() throws JSONException {
        characterNames.add(getString(R.string.select_character));
        Iterator<String> iterable = book.keys();
        while (iterable.hasNext()) {
            String characterName = iterable.next();
            JSONObject character = (JSONObject) book.get(characterName);
            characterNames.add(characterName + " (" + character.get("class").toString() + ")");
        }
    }

    private final View.OnClickListener openCharacterNameDialog = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            AlertDialog dialog = new AlertDialog.Builder(Start.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialog_characterName = inflater.inflate(R.layout.dialog_character_name, null);
            dialog.setView(dialog_characterName);
            CharacterNameListener characterNameListener = new CharacterNameListener(book, Start.this, dialog);
            Button characterNameOk = dialog_characterName.findViewById(R.id.characterNameOk);
            characterNameOk.setOnClickListener(characterNameListener);
            Button characterNameCancel = dialog_characterName.findViewById(R.id.characterNameCancel);
            characterNameCancel.setOnClickListener(characterNameListener);
            Spinner characterClassesSpinner = dialog_characterName.findViewById(R.id.characterClasses);
            List<String> characterClasses = Arrays.asList("Bandito", "Gunslinger", "Indian scout", "Lawman", "Preacher", "Rancher", "Saloon girl", "U.S. marshal");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Start.this, android.R.layout.simple_spinner_dropdown_item, characterClasses);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            characterClassesSpinner.setAdapter(adapter);
            EditText characterNameInput = dialog_characterName.findViewById(R.id.characterNameInput);
            dialog.show();
            characterNameInput.requestFocus();
            characterNameInput.postDelayed(() -> {
                InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(characterNameInput,0);
            },200);
        }
    };

    private final AdapterView.OnItemSelectedListener spinnerOnItemSelect = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (spinnerTouched && position > 0) {
                Log.d("spinner", "clicked");
                Iterator<String> characterNamesList = book.keys();
                String characterName = null;
                for (int i = 0; i < position && characterNamesList.hasNext(); i++) {
                    characterName = characterNamesList.next();
                }
                if (characterName != null && characterName.matches("[\\p{L}\\s\\-'`´,&()!]*")) {
                    Intent intent = new Intent(Start.this, CharacterSheet.class);
                    intent.putExtra("characterName", characterName);
                    intent.putExtra("book", book.toString());
                    Start.this.startActivity(intent);
                }
            }
            Spinner spinner = findViewById(R.id.characterNames);
            spinner.setSelection(0, false);
            spinnerTouched = false;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private String readFromFile() {
        String ret = "";
        try {
            InputStream inputStream = getBaseContext().openFileInput("sidebag.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("Initialization", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Initialization", "Can not read file: " + e.toString());
        }
        return ret;
    }
}
