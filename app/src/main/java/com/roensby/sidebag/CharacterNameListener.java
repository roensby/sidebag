package com.roensby.sidebag;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

public class CharacterNameListener implements View.OnClickListener {

    JSONObject book;

    Context context;

    AlertDialog dialog;

    public CharacterNameListener(JSONObject incomingBook, Context incomingContext, AlertDialog incomingDialog) {
        book = incomingBook;
        context = incomingContext;
        dialog = incomingDialog;
    }

    @Override
    public void onClick(View view) {
        View parentView = (View) view.getParent();
        EditText characterNameInput = parentView.findViewById(R.id.characterNameInput);
        Spinner characterClassesSpinner = parentView.findViewById(R.id.characterClasses);
        characterNameInput.setTextColor(Color.BLACK);
        int buttonId = view.getId();
        if (buttonId == R.id.characterNameOk) {
            String characterName = characterNameInput.getText().toString().replace("\n", "").replace("\r", "").trim();
            if (characterName.matches("[\\p{L}\\s\\-'`´,&()!]+")) {
                try {
                    book.put(characterName, new JSONObject());
                    JSONObject characterNameObject = (JSONObject) book.get(characterName);
                    characterNameObject.put("class", characterClassesSpinner.getSelectedItem().toString());
                    characterNameInput.setText("");
                    Intent intent = new Intent(context, CharacterSheet.class);
                    intent.putExtra("characterName", characterName);
                    intent.putExtra("book", book.toString());
                    dialog.dismiss();
                    context.startActivity(intent);
                } catch (JSONException e) {
                    Log.e("CharacterName", "JSON error: " + e.toString());
                }
            }
            else {
                characterNameInput.setTextColor(Color.RED);
            }
        }
        else {
            dialog.cancel();
        }
    }
}
